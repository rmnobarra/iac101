# whoami

* +15 anos experiência
* network
* linux
* containers
* ansible
* terraform
* vagrant
* chef
* shell script
* python

# intro

Dificilmente uma empresa hoje em dia está fora do ciclo de construir, manter ou consumir sistemas. Informações rápidas e consistentes equivalem a ganhar ou perder dinheiro, e os dados possuem valores muitas vezes, incalculáveis. 

Por isso, equipes de desenvolvimento tem a infinita tarefa de entregar novos sistemas com maior frequência enquanto infraestrutra,
estar preparado para atender essa demanda.

# motivação

Há alguns anos, provisionar infraestrutura era terrivelmente penoso, comprar hardware, atualizar firmwares, energizar, cabear, provisionar rede, storage, s.o, atualizações de S.O e ai começava configurar a aplicação.
Sysadmins dessa época sempre tinham a mão scripts em shell miraculosos, que configuravam, coletava informação, fazia até chover. O problema é que não escala, é díficil
manter histórico e em algum momento aquele "awk com sed + pipe grep" irá te deixar na mão.

# o que é

A infraestrutura como código (IaC) envolve a substituição de processos manuais e procedimentos operacionais padrão para configurar dispositivos e sistemas operacionais de hardware distintos por código ou aplicativo que gerencie e provisione automaticamente a pilha de tecnologia. 

Uma infraestrutura típica consiste em componentes como bancos de dados, servidores, aplicativos e redes interligando tudo o que foi mencionado acima. No IaC, também conhecido como infraestrutura como código, você pode configurar e implantar esses componentes de infraestrutura mais rapidamente com consistência, tratando-os como um aplicativo.

Portanto, toda vez que você precisa configurar uma infraestrutura, não precisa ir até o time de infraestrutura, fazer uma solicitação, criar um ticket e aguardar a participação deles. Em vez disso, seus desenvolvedores e equipes de operações podem fazer isso facilmente usando código.

Assim como qualquer processo de automação, os benefícios óbvios aqui são custo, simplicidade e velocidade. Como com o IaC, os Sysadmins podem trabalhar em tarefas com mais prioridade e os desenvolvedores, juntamente com os processos do DevOps, podem trabalhar com mais eficiência. 

Você pode atender aos requisitos de infraestrutura mais dinamicamente do que nunca e pode literalmente "subir" um ambiente inteiro executando um único pedaço de código. Assim, aumentando a velocidade do processo geral, reduzindo os esforços manuais e o tempo necessário para fornecer os requisitos de infraestrutura. 

Da mesma forma, você pode "destruir" ambientes executando um script que pode economizar fortunas associadas aos recursos dos quais talvez você não precise mais.

# vantagens

## segurança

Quando você está configurando uma infraestrutura enorme com apenas um clique, torna-se muito importante que suas equipes de segurança sejam igualmente responsivas e garantam que o ambiente definido seja suficientemente seguro. Isso pode não ser possível todas as vezes, o que pode gerar questões sobre segurança no IaC. Mas aqui está o problema: 

Antes da implantação do código para Infraestrutura, você pode executar testes de unidade para segurança e conformidade tratando o código como uma aplicação ou como qualquer outro código. A implantação e o teste em ambientes de área restrita podem falar sobre as possíveis ameaças; portanto, você pode cuidar delas com antecedência. O monitoramento contínuo de ambientes ativos com serviços de segurança em nuvem pode ajudar nos testes contínuos de segurança e conformidade. Todas essas práticas em vigor reduzirão drasticamente o tempo para identificar ameaças, tratá-las e a complexidade operacional.

Outra vantagem na área de segurança é que você pode documentar e rastrear as alterações feitas no ambiente. Isso ajudará não apenas do ponto de vista da segurança, mas também reduzirá o risco associado à dependência de pessoas associadas à administração.

## eficiência e consistência

Com o IaC, seus desenvolvedores podem trabalhar de forma mais produtiva devido ao aumento da eficiência e à dependência reduzida de outras equipes. Eles podem iniciar seus próprios ambientes como e quando necessário. Com um código, eles podem cuidar de várias etapas que estariam lá sem a automação. Scripts pré-configurados tornarão o processo de rotação sem problemas. E, assim como o spin-up, você pode automatizar o spin-down que reduzirá o número de componentes que não estão em uso, ajudando a manter seus ambientes de nuvem limpos e organizados.

Definir procedimentos padrão e scripts predefinidos pode ajudar a manter a consistência nos procedimentos de implantação. Além disso, com o IaC, a intervenção humana é mínima, o que significa erros humanos reduzidos e maior uniformidade e padronização. Como resultado, os problemas como compatibilidade e ele é executado na minha máquina são reduzidos ao mínimo.

Agora que você conhece as vantagens associadas à Infraestrutura como código, convém começar com ela na sua organização. Mas para isso, você precisa entender as melhores práticas e as melhores ferramentas para o mesmo. Portanto, no meu próximo blog, falarei sobre as melhores práticas e as ferramentas para Infraestrutura como código.

# na prática

Conceitualmente, uma infraestrutura configurada como código, chega a ser útopica de tão atraente, para materializar o conceito de iac, algumas ferramentas se fazem necessárias

*DISCLAIMER: é possível fazer qualquer coisa com qualquer coisa, a escolha das ferramentas foi balizada de acordo com o tamanho da comunidade, maturidade e suporte por parte do fabricante*

## terraform

Terraform é uma ferramenta para construir, modificar e versionar infraestruturas criada para atuar principalmente com as clouds como Azure, Digital Ocean, AWS, GCP, Oracle Cloud, VMWare Cloud. bem como outras diversas ferramentas DevOps.

Caracteristicas:

* É agnostica a clouds;
* Linguagem Própria ( HCL - Hashicorp Configuration Language);
* Executa o planejamento de execução antes da aplicação da configuração;
* Interage com as Clouds mais utilizadas no mercado;
* Integração com outras ferramentas de Infrastructure as code.

## vagrant

O Vagrant é uma ferramenta para criar e gerenciar ambientes de máquinas virtuais em um único fluxo de trabalho. Com um fluxo de trabalho fácil de usar e foco na automação,o Vagrant reduz o tempo de configuração do ambiente de desenvolvimento, aumenta a paridade da produção e faz com que os "trabalhos na minha máquina" desculpem uma relíquiado passado.

Caracteristicas:

* Ele pode gerenciar o ciclo de vida de um projeto.
* É possível compartilhar máquinas.
* Ajuda na criação de máquinas virtuais em seu ambiente local.
* Facilita o provisionamento de máquinas virtuais com um script de shell ou ferramentas como chef ou ansible.

## ansible

O Ansible é uma ferramenta de automação criada para gerenciar múltiplas máquinas de uma única vez através de playbooks escritas no formato YAML.

Caracteristicas:

* Arquitetura Master to Nodes;
* Linguagem de Configuração Simples;
* Não necessita de agents;
* Simples de realizar manutenção;
* Curva de aprendizado curta (muito);

O Ansible é uma ferramenta muito poderosa, uma vez que ela necessita apenas de confiança SSH (seja por chave ou senha) e Python, o que a maioria das máquinas linux já possúi por padrão.

# Demo 1

## Vagrant

vagrant validate: lint syntax
vagrant up: constroi o ambiente baseado no Vagrantfile
vagrant ssh <host>: conecta via ssh no host
vagrant halt: shutdown ambiente
vagrant destroy: deleta o ambiente

# Demo 2

## Terraform

Infraestrutura com 1 load balance apontando para 2 web servers

## Comandos:

terraform init: Baixa dependencias necessárias para execução

terraform validate: lint de syntax

terraform plan: exibe o que será aplicado na estrutura

terraform apply: aplica a configuração na estrutura

terraform output: exibe valores de output configurados no projeto. É possivel efetuar querys usando jq.

Ex.

terraform output -json apps_ip_external | jq -r '.[]'
terraform output -json apps_ip_external | jq -r '.[0]'
terraform output -json apps_ip_external | jq -r '.[1]'

# Demo 3

Configurando a estrutura acima com ansible

Comandos:

ping: Valida a conexão com o destino

Ex:
ansible app -m ping
ansible lb -m ping
ansible all -m ping

ansible-playbook playbook.yaml --syntax-check: lint syntax
ansible-playbook playbook.yaml --user=`whoami`
ansible-playbook playbook.yaml --user=`whoami` --start-at-task="Executa nginx"

# Conclusão

Iac age muitas vezes em hosts não efêmeros na infra estrutura, conectando e mantendo o estado desejado nas instâncias, esse método já é considerado por muitos
como anti pattern, pois a fluxos aonde instãncias são destruídas e substitúidas por novas imagens, em deploys glue /green, canary, etc.

O ponto de reflexão é que infraestrutura, desenvolvimento e correlatos persegue a linha do horizonte em termos de excelência, uma melhoria continua.
Novas ferramentas surgem todo o dia, tornando o hoje obsoleto, movendo camadas de conhecimento em commodities. Saber linux, docker, kubernetes
não passa a ser um diferencial e conhecer pelo menos uma linguagem de programação, tão fundamental quanto.

Em um futuro não muito longe, muitos cargos serão extintos, e muitos mais serão criados. Uma vertente acredita que todas as áreas serão derivadas de desenvolvedores,
como desenvolvedor de infra estrutura, e é nó que acredito.

Muitas habilidades serão necessárias para as melhores vagas ao mesmo tempo que o conhecimento se torna cada vez mais acessível.

Planeje, estude e conquiste.

# Links

Twitter:
https://twitter.com/rmnobarra


Canal devops telegram:

https://t.me/novodevopsbr

