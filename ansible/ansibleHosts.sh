# export de variáveis

APPS_IPS=`terraform output -json -state=../terraform/terraform.tfstate apps_ip_external | jq -r '.[]'`
APP_IP_01=`terraform output -json -state=../terraform/terraform.tfstate apps_ip_external | jq -r '.[0]'`
APP_IP_02=`terraform output -json -state=../terraform/terraform.tfstate apps_ip_external | jq -r '.[1]'`
LB_IP=`terraform output -json -state=../terraform/terraform.tfstate lb_ip_external | jq -r ''`
# configura ansible hosts

echo "[app]" > hosts
printf '%s\n' $APPS_IPS >> hosts

echo "" >> hosts
echo "[lb]" >> hosts
echo $LB_IP >> hosts

cat env_variables_base > env_variables
echo "app_ip_01: $APP_IP_01" >> env_variables
echo "app_ip_02: $APP_IP_02" >> env_variables
echo "fqdn: $LB_IP" >> env_variables