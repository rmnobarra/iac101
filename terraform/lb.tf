// load balance

resource "google_compute_address" "lb-ip-static" {
  name = "${var.lb_01_ip_name}"
}

output "lb_ip_external" {
  value = "${google_compute_instance.lb.network_interface.0.access_config.0.nat_ip}"
}

output "lb_ip_internal" {
  value = "${google_compute_instance.lb.network_interface.0.network_ip}"
}

resource "google_compute_instance" "lb" {
  name         = "${var.lb_01_name}"
  machine_type = "custom-1-1024"
  zone         = "${var.gcp_zone}"
  labels = {
    role = "lb"
    product = "infra",
    environment = "non-prod"
  }


  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
    }
  }
  network_interface {
    network = "default"
    access_config{
      nat_ip = "${google_compute_address.lb-ip-static.address}"
    }
  }

  tags = ["devweek"]
}

  output "lb_name" {
    value = "${google_compute_instance.lb.name}"
  }
