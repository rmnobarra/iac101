#!/bin/bash

#METADADOS TERRAFORM
#export TERRAFORM_FILE="tfstate"

##HOSTNAME WORKER, MASTER, LOAD BALANCE
#export WORKER_HOSTNAME=$(jq -r '. | .resources[].instances[].attributes.name | select( contains("-worker")) ' "$TERRAFORM_FILE" | uniq)
#export MASTER_HOSTNAME=$(jq -r '. | .resources[].instances[].attributes.name | select( contains("-master")) ' "$TERRAFORM_FILE" | uniq)
#export LB_HOSTNAME=$(jq -r '. | .resources[].instances[].attributes.name | select( contains("-lb")) ' "$TERRAFORM_FILE" | uniq)

##IP WORKER, MASTER, LOAD BALANCE
#export WORKER_IP=$(jq -r '. | .resources[].instances[].attributes | select(any(.name; contains("-worker"))) | if .network_interface == null then empty else .network_interface[].network_ip end' "$TERRAFORM_FILE"  | uniq)
#export MASTER_IP=$(jq -r '. | .resources[].instances[].attributes | select(any(.name; contains("-master"))) | if .network_interface == null then empty else .network_interface[].network_ip end' "$TERRAFORM_FILE"  | uniq)
#export LB_IP=$(jq -r '. | .resources[].instances[].attributes | select(any(.name; contains("-lb"))) | if .network_interface == null then empty else .network_interface[].network_ip end' "$TERRAFORM_FILE"  | uniq)


#MASTER 01
#export MASTER_01_HOSTNAME=$(jq -r '. | .resources[].instances[0].attributes.name | select( contains("-master")) ' "$TERRAFORM_FILE" | uniq)
#export MASTER_01_IP=$(jq -r '. | .resources[].instances[0].attributes | select(any(.name; contains("-master"))) | if .network_interface == null then empty else .network_interface[].network_ip end' "$TERRAFORM_FILE" | uniq)

#MASTER 01 e 02
#export MASTER_01_HOSTNAME=$(jq -r '. | .resources[].instances[1:5].attributes.name | select( contains("-master")) ' "$TERRAFORM_FILE" | uniq)
#export MASTER_01_IP=$(jq -r '. | .resources[].instances[0].attributes | select(any(.name; contains("-master"))) | if .network_interface == null then empty else .network_interface[].network_ip end' "$TERRAFORM_FILE" | uniq)

####################################################################################

#EXPORTA ENV
echo "export TFSTATE_PATH=$PWD/terraform.tfstate" >> ~/.bashrc

