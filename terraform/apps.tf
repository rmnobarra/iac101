// apps
resource "google_compute_address" "app-static-ip" {
    count = var.app_count_ip
    name = "${var.app_static_ip_name}-${count.index}"
    //"${var.hostnames[count.index]}"
}

output "apps_ip_external" {
  value = "${google_compute_instance.apps.*.network_interface.0.access_config.0.nat_ip}"
}
output "apps_ip_internal" {
  value = "${google_compute_instance.apps.*.network_interface.0.network_ip}"
}

resource "google_compute_instance" "apps" {
  count = var.app_count
  name         = "${var.app_name}-${count.index +1}"
  machine_type = "custom-1-2048"
  zone         = var.gcp_zone
  labels = {
  role = "app",
  vm_type = "non-preem",
  product = "infra",
  environment = "non-prod"
  }

  boot_disk {
    initialize_params {
      image = "centos-cloud/centos-7"
      size = 50
    }
  }

  network_interface {
      network = "default"
      access_config{
          nat_ip = element(google_compute_address.app-static-ip.*.address, count.index +1)
    }
  }

  tags = ["devweek"]
}
  output "apps_name" {
    value = "${google_compute_instance.apps.*.name}"
  }