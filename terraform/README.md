# Terraform k8s tools
## Projeto Terraform para criação do ambiente kubernetes.
### Configura:
1. 1x Load Balance TCP para API + http / https workers;
2. 3x Control planes
3. 2x Worker nodes
4. 3x Storage GlusterFS

#Préreqs:
1. Terraform v0.12.0 +
2. Token .json do projeto GCP
3. Chave pública do usuário sysadmin

# Modo de usar

## Para download dos módulos necessários
```bash
terraform init
```
## Para aplicar as configurações no ambiente (Digite "yes" quando solicitado ou execute com "--auto-approve")
```bash
terraform apply
```

## Para finalizar
```bash
./export.sh
```

## terraform outputs
IPS_WORKERS=`terraform output -state=$TFSTATE_PATH workers_ip`
terraform output workers_ip
terraform output -json workers_ip | jq '.[1]'
terraform output -json workers_ip | jq -r '.[1]'