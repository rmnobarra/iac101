//Define projeto
variable gcp_project {
    type = string
    default = "poc-pd"
}
//Define região
variable gcp_region {
    type = string
    default = "us-east1"
}

//Define Zona
variable gcp_zone {
    type = string
    default = "us-east1-b"
}

//instancias apps count
variable "app_count" {
    default = 2
}

//instancia ip estatico apps count
variable "app_count_ip" {
    default = 2
}

variable "lb_01_ip_name" {
    type = string
    default = "lb-01"
}

variable "lb_01_name" {
    type = string
    default = "lb-01"
}


variable "app_static_ip_name" {
    type = string
    default = "app-static-ip"
}

variable "app_name" {
    type = string
    default = "app"
}

//instancias apps count
variable "app-preem_count" {
    default = 2
}