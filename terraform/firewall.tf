resource "google_compute_firewall" "devweek-externo" {
  name    = "devweek-externo"
  network = "default"
  project = "poc-pd"

  allow {
    protocol = "tcp"
    ports    = ["80","8081","22"]
  }
  target_tags = ["devweek"]
  source_ranges = ["179.191.95.141",
  "${google_compute_instance.lb.network_interface.0.access_config.0.nat_ip}"
  ]
}

resource "google_compute_firewall" "devweek-interno" {
  name    = "devweek-externo-interno"
  network = "default"
  project = "poc-pd"

  allow {
    protocol = "all"
  }
  source_tags = ["devweek"]
  target_tags = ["devweek"]
}