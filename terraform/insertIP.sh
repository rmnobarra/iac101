#workers
ip_worker1=`terraform output -json workers_ip_external | jq -r '.[0]'`
ip_worker2=`terraform output -json workers_ip_external | jq -r '.[1]'`
sed -i "s/179.191.95.141\"\,/&\"$ip_worker1\"\,/" firewall.tf
sed -i "s/179.191.95.141\"\,/&\"$ip_worker2\"\,/" firewall.tf

#masters
ip_master1=`terraform output -json masters_ip_external | jq -r '.[0]'`
ip_master2=`terraform output -json masters_ip_external | jq -r '.[1]'`
ip_master3=`terraform output -json masters_ip_external | jq -r '.[2]'`
sed -i "s/179.191.95.141\"\,/&\"$ip_master1\"\,/" firewall.tf
sed -i "s/179.191.95.141\"\,/&\"$ip_master2\"\,/" firewall.tf
sed -i "s/179.191.95.141\"\,/&\"$ip_master3\"\,/" firewall.tf


